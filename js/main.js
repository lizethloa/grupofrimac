var isMobile = false;
var isDesktop = false;


$(window).on("load resize",function(e){

		
		
		//mobile detection
		if(Modernizr.mq('only all and (max-width: 767px)') ) {
			isMobile = true;
		}else{
			isMobile = false;
		}


		//tablette and mobile detection
		if(Modernizr.mq('only all and (max-width: 1024px)') ) {
			isDesktop = false;
		}else{
			isDesktop = true;
		}
        toTop(isMobile);
});


/*
|--------------------------------------------------------------------------
| DOCUMENT READY
|--------------------------------------------------------------------------
*/  
$(document).ready(function() {


/*
|--------------------------------------------------------------------------
| APPEAR
|--------------------------------------------------------------------------
*/
if($('.activateAppearAnimation').length){
	nekoAnimAppear();


	$('.reloadAnim').click(function (e) {

		$(this).parent().parent().find('img').removeClass().addClass('img-responsive');

		nekoAnimAppear();
		e.preventDefault();
	});
}


/*
|--------------------------------------------------------------------------
| REVOLUTION SLIDER
|--------------------------------------------------------------------------
*/ 
if($('#rsDemoWrapper').length){


    	$('.tp-banner').revolution(
                {
                    delay:12000,
                    startwidth:1920,
                    startheight:1080,
                    hideThumbs:10,
					navigationType:"none",
                    fullWidth:"on",
                    forceFullWidth:"on"
                });

    	$('#rsDemoWrapper').css('visibility', 'visible');
    }


/*
|--------------------------------------------------------------------------
| OWL CAROUSEL
|--------------------------------------------------------------------------
*/ 

  var owl_slide = $("#owl-slide");
 
  owl_slide.owlCarousel({
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
	  pagination : false,
	  //Basic Speeds
      slideSpeed : 1200,
      paginationSpeed : 1800,
      rewindSpeed : 1000,
      //Autoplay
      autoPlay : true,
      stopOnHover : false,
  });
  
// Custom Navigation Events
    $(".owl-slide-arrows .next").click(function() {
        owl_slide.trigger('owl.next');
    });
	
    $(".owl-slide-arrows .prev").click(function() {
        owl_slide.trigger('owl.prev');
    });
	
	/**/
	
	 var owl_1999 = $("#owl-1999");
 
  owl_1999.owlCarousel({
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
	  pagination : false,
	  //Basic Speeds
      slideSpeed : 1200,
      paginationSpeed : 800,
      rewindSpeed : 1000,
      //Autoplay
      autoPlay : false,
      stopOnHover : false,
  });
  
// Custom Navigation Events
    $(".owl-1999-arrows .next").click(function() {
        owl_1999.trigger('owl.next');
    });
	
    $(".owl-1999-arrows .prev").click(function() {
        owl_1999.trigger('owl.prev');
    });
	
	/**/
	
	 var owl_2006 = $("#owl-2006");
 
  owl_2006.owlCarousel({
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
	  pagination : false,
	  //Basic Speeds
      slideSpeed : 2200,
      paginationSpeed : 1800,
      rewindSpeed : 2000,
      //Autoplay
      autoPlay : false,
      stopOnHover : false,
  });
  
// Custom Navigation Events
    $(".owl-2006-arrows .next").click(function() {
        owl_2006.trigger('owl.next');
    });
	
    $(".owl-2006-arrows .prev").click(function() {
        owl_2006.trigger('owl.prev');
    });
	/**/
	
	 var owl_2011 = $("#owl-2011");
 
  owl_2011.owlCarousel({
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
	  pagination : false,
	  //Basic Speeds
      slideSpeed : 1200,
      paginationSpeed : 1800,
      rewindSpeed : 1000,
      //Autoplay
      autoPlay : false,
      stopOnHover : false,
  });
  
// Custom Navigation Events
    $(".owl-2011-arrows .next").click(function() {
        owl_2011.trigger('owl.next');
    });
	
    $(".owl-2011-arrows .prev").click(function() {
        owl_2011.trigger('owl.prev');
    });
	
		/**/
	
	 var owl_2017 = $("#owl-2017");
 
  owl_2017.owlCarousel({
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
	  pagination : false,
	  //Basic Speeds
      slideSpeed : 2200,
      paginationSpeed : 800,
      rewindSpeed : 2000,
      //Autoplay
      autoPlay : false,
      stopOnHover : false,
  });
  
// Custom Navigation Events
    $(".owl-2017-arrows .next").click(function() {
        owl_2017.trigger('owl.next');
    });
	
    $(".owl-2017-arrows .prev").click(function() {
        owl_2017.trigger('owl.prev');
    });
	
	
	/**/
	
  var owl_slide2 = $("#owl-slide2");
 
  owl_slide2.owlCarousel({
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1000,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,2], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
	  pagination : false,
	  //Basic Speeds
      slideSpeed : 1200,
      paginationSpeed : 1800,
      rewindSpeed : 1000,
      //Autoplay
      autoPlay : true,
      stopOnHover : false,
  });
  
// Custom Navigation Events
    $(".owl-slide2-arrows .next").click(function() {
        owl_slide2.trigger('owl.next');
    });
	
    $(".owl-slide2-arrows .prev").click(function() {
        owl_slide2.trigger('owl.prev');
    });
	
	
	
	/**/
	
  var owl_servicios = $("#owl-servicios");
 
  owl_servicios.owlCarousel({
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1000,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,2], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
	  pagination : false,
	  //Basic Speeds
      slideSpeed : 1200,
      paginationSpeed : 1800,
      rewindSpeed : 1000,
      //Autoplay
      autoPlay : true,
      stopOnHover : false,
  });
  
// Custom Navigation Events
    $(".owl-servicios-arrows .next").click(function() {
        owl_servicios.trigger('owl.next');
    });
	
    $(".owl-servicios-arrows .prev").click(function() {
        owl_servicios.trigger('owl.prev');
    });
	
	
//funcion owl clientes

  var owl_cliente = $("#owl-cliente");
 
  owl_cliente.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,2], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option	
      slideSpeed : 200,
      paginationSpeed : 800,
      rewindSpeed : 1000,
      //Autoplay
      autoPlay : true,
      stopOnHover : false,
  });
   //SYNCL owlCarousel
 
	 var sync1 = $("#sync1");
  var sync2 = $("#sync2");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: true,
    pagination:false,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 8,
    itemsDesktop      : [1199,5],
    itemsDesktopSmall     : [979,3],
    itemsTablet       : [768,3],
    itemsMobile       : [479,2],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  };
 
//END DOCUMENT READY   
});

$('.dropdown-toggle').click(function(e) {
  if ($(document).width() > 768) {
    e.preventDefault();

    var url = $(this).attr('href');

       
    if (url !== '#') {
    
      window.location.href = url;
    }

  }
});
/*
|--------------------------------------------------------------------------
| EVENTS TRIGGER AFTER ALL IMAGES ARE LOADED
|--------------------------------------------------------------------------
*/
$(window).load(function() {

"use strict";
    
    /*
    |--------------------------------------------------------------------------
    | PRELOADER
    |--------------------------------------------------------------------------
    */
    if($('#status').length){
        $('#status').fadeOut(); // will first fade out the loading animation
        $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
        $('body').delay(350).css({'overflow':'visible'});
    }


    /*
    |--------------------------------------------------------------------------
    | MEGA MENU CONTACTO
    |--------------------------------------------------------------------------
    */

 $(document).on('click', '.navbar .dropdown-menu', function(e) {e.stopPropagation();});


//END WINDOW LOAD
});


/*
|--------------------------------------------------------------------------
| FUNCTIONS
|--------------------------------------------------------------------------
*/

/* Appear function */
function nekoAnimAppear(){
	$("[data-nekoAnim]").each(function() {

		var $this = $(this);

		$this.addClass("nekoAnim-invisible");
		
		if($(window).width() > 767) {
			
			$this.appear(function() {

				var delay = ($this.data("nekodelay") ? $this.data("nekodelay") : 1);
				if(delay > 1) $this.css("animation-delay", delay + "ms");

				$this.addClass("nekoAnim-animated");
				$this.addClass('nekoAnim-'+$this.data("nekoanim"));

				setTimeout(function() {
					$this.addClass("nekoAnim-visible");
				}, delay);

			}, {accX: 0, accY: -150});

		} else {
			$this.addClass("nekoAnim-visible");
		}
	});
}




/* TO TOP BUTTON */

function toTop(mobile){
    
   if(mobile == false){

        if(!$('#nekoToTop').length)
        $('body').append('<a href="#" id="nekoToTop"><i class="fas fa-arrow-up"></i></a>');

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#nekoToTop').slideDown('fast');
            } else {
                $('#nekoToTop').slideUp('fast');
            }
        });

        $('#nekoToTop').click(function (e) {
            e.preventDefault();
            $("html, body").animate({
                scrollTop: 0
            }, 800, 'easeInOutCirc');
            
        });
   }else{

        if($('#nekoToTop').length)
        $('#nekoToTop').remove();

    }

}






